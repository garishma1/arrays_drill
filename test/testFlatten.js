let flatten = require("../flatten")

let nestedArray = [1, [2], [[3]], [[[4]]]]

let flattened = flatten(nestedArray)

console.log(flattened)