function flatten(elements){
    if(Array.isArray(elements)){
        let flattenedArray=[]
    elements.forEach((element=>{
        if(Array.isArray(element)){
            flattenedArray=flattenedArray.concat((flatten(element)))
        }else{
            flattenedArray.push(element)
        }
    }))
    return flattenedArray
    }else{
        return null
    }
}
    
    module.exports = flatten

