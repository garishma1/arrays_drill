function reduce(elements,cb,startingValue){
    if(Array.isArray(elements)){
        let accumulator = startingValue===undefined?elements[0]:startingValue
    let startIndex = startingValue !== undefined ? 0 : 1;

    for(let index =startIndex;index<elements.length;index++){
        accumulator = cb(accumulator,elements[index],index,elements)
    }
    return accumulator
    }else{
        return null
    }
   
}
module.exports = reduce