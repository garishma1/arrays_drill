function map(elements,cb){
    if(Array.isArray(elements)){
        let mapArray=[]
    for(let index = 0;index<elements.length;index++){
        mapArray.push(cb(elements[index],index,elements))
    }
    return mapArray
    }else{
        return null
    }
}

module.exports = map