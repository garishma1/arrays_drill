function filter(elements,cb){
    if(Array.isArray(elements)){
        let newArray = []
    for(let index = 0;index<elements.length;index++){
        if(cb(elements[index],index,elements)){
            newArray.push(elements[index])
        }
    }
    return newArray
    }else{
        return null
    }
}
    

module.exports = filter